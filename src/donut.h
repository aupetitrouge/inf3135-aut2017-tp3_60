#ifndef DONUT_H
#define DONUT_H

#include <SDL2/SDL.h>
#include "utils.h"
#include "map.h"
#include "animated_spritesheet.h"

// --------------- //
// Data structures //
// --------------- //

struct Donut {
	struct AnimatedSpritesheet *animatedSpritesheet;// His spritesheet
	struct Point screenPosition;					// His position in the screen
	struct Point mapPosition;						// His position in the map
	bool found;										// Status if the donut was found
	SDL_Renderer *renderer;							// The renderer
	SDL_Rect hitbox;								// The donut's hitbox
};

// --------- //
// Functions //
// --------- //

/**
 * Creates the donut.
 *
 * @param renderer	The renderer
 * @return			A pointer to the donut, NULL if there was an error;
 *					Call IMG_GetError() for more information.
 */
struct Donut *Donut_create(SDL_Renderer *renderer);

/**
 * Deletes the donut.
 *
 * @param donut  The donut to delete
 */
void Donut_delete(struct Donut *donut);

/**
 * Renders the donut.
 *
 * @param donut  The donut to render
 */
void Donut_render(struct Donut *donut);

/**
 * Assign coordonate to a donut
 *
 * @param x the x coordonate base on the map
 * @param y the y coordonate base on the map
 * @param donut The donut to compare
 */
void Donut_location_assigner(double x, double y, struct Donut *donut);
#endif
